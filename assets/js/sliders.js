/*Slider intermedia*/
$('[owl-otros-destinos]').owlCarousel({
  items: 2,
  loop: false,
  center: true,
  margin: 30,
  responsive: {
    0: {

      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items: 2.5
    }
  }
})

/* Galería Interna de Paquete */

function enableGaleriaPaquete() {
  var galeriaOwl = $('[galeria-owl]').owlCarousel({
    items: 1,
    loop: false,
    URLhashListener:true,
    autoPlay: false,
    startPosition: 'URLHash',
    dots: true,
  })

  $('[galeria-owl-bullet]').on('click', function(e) {
    e.preventDefault();
    galeriaOwl.trigger('to.owl.carousel', [$(this).index()]);

  })
}

enableGaleriaPaquete();
